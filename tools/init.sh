#!/bin/sh
set -a

DIR=$1
[ -n "$DIR" ] || DIR=./config

GUI=false
if [ -n "$DISPLAY" ]; then
  GUI=true
fi

[ -s /etc/lsb-release ] && . /etc/lsb-release
OS_ARCH=$(dpkg --print-architecture)

for TEMPLATE in $DIR/*; do
  FILE=${TEMPLATE%.tmpl}
  FILE=${FILE##*/}

  echo $FILE
  envsubst < $TEMPLATE > $FILE
done
