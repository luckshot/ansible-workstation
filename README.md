# Ansible Workstation

This is a repository of Ansible playbooks to make setting up new
computer easy.

## Getting Started

To create the intial `ansible.cfg` and `inventory.cfg` files run the
init script.

``` bash
$ ./tools/init.sh
```

This will populate the files with local config information.

## Installing Ansible

The shipped version of Ansible can be out-of-date. It’s best to have a
local install in a virtual environment.

``` bash
apt install python3-venv, python3-pip
python3 -m venv .venv
. .venv/bin/activate
pip install --upgrade ansible
```

The packages installed are also managed by the playbooks, but the manual
install has to be done to use Ansible.

## What’s Installed

The playbooks are devided into two sections, files and Ubuntu.

### Files

The plays in the Files playbook configure the basics of the system. This
includes things like setting up directories, downloading and installing
apps, and setting the `update-alternatives` links to point to the most
useful versions.

Apps are saved to `~/.local/alternatives` and the alias are stored in
`~/.local/bin` to keep the top level `~/bin` free of clutter.

The versions of the apps can be found in
`playbooks/files/vars/versions.yml`. Multiple versions of some are
included, like `kubectl` because kubernetes.

Also included are some small script downloads for utility functions,
these are stored in `~/bin`.

### Ubuntu

The plays in the Ubuntu playbook setup the basic files needed for a
productive Ubuntu-based environment. The init script will check for the
`$DISPLAY` environment variable and include GUI apps if it is present.

The list of installed packages can be found in the
`playbooks/ubuntu/vars/packages.yml` file.

## AutoFS

This allows automatic mounting of the local NFS shares. Manual setup is
best as it might vary between machines.

    sudo mkdir /etc/auto.master.d
    sudo touch /etc/auto.master.d/warez.autofs
    sudo touch /etc/auto.media

In the `/etc/auto.media` file, add any of:

    warez	-fstype=nfs4	warez.local:/opt/share
    keepass	-fstype=nfs4	warez.local:/opt/share/keepass
    music	-fstype=nfs4	warez.local:/opt/share/music

In `/etc/auto.master.d/warez.autofs` file add:

    /media	/etc/auto.media
